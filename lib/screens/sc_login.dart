import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ridesafety/models/loginModel.dart';
import 'package:ridesafety/screens/sc_home.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/commonFiles/RaiseButton.dart';
import 'package:ridesafety/utils/commonFiles/TextField.dart';
import 'package:ridesafety/utils/commonFiles/common.dart';
import 'package:ridesafety/utils/commonFiles/show_dialog.dart';
import 'package:ridesafety/utils/commonFiles/widget_spacer.dart';
import 'package:ridesafety/utils/constants.dart';
import 'package:ridesafety/utils/font.dart';
import 'package:ridesafety/utils/network_repository.dart';
import 'package:ridesafety/utils/string.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  final mobileNumberController = TextEditingController();
  final mobileNumberFocus = new FocusNode();
  final otpController = TextEditingController();
  bool _isLoading = false;
  bool _isLoginScreen = true;
  LoginModel loginModel;

  void SendOTPValidation() {
    mobileNumberFocus.unfocus();
    if (mobileNumberController.text.isNotEmpty) {
      sendOTP(context);
    } else {
      ShowDialog.dialogs(context, STRINGS.please_enter_your_MobileNo);
    }
  }

  void VarifyOTPValidation() {
    mobileNumberFocus.unfocus();
    if (otpController.text.isNotEmpty) {
      if (otpController.text == loginModel.data.defOTP ||
          otpController.text == loginModel.data.oTP) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Home()),
            (Route<dynamic> route) => false);
      } else {
        ShowDialog.dialogs(context, STRINGS.enter_valid_OTP);
      }
    } else {
      ShowDialog.dialogs(context, STRINGS.enter_your_OTP);
    }
  }

/*----------API Calling----------------*/
  void sendOTP(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        FocusScope.of(context).requestFocus(FocusNode());
        _isLoading = true;
      });

/*----------API url----------------*/
      String url = STRINGS.APISendOTP + "/" + mobileNumberController.text;

      String response = await callGetMethod(url, null);
/*----------API Response----------------*/
      loginModel = LoginModel.fromJson(json.decode(response));
      _isLoading = false;

      if (loginModel.status == STRINGS.STATUS_SUCCESS) {
        setState(() {
          _isLoginScreen = false;
        });
        // /*----------sharedPreferences Data Store----------------*/
        sharedPreferences = await SharedPreferences.getInstance();

        sharedPreferences.setString(
            STRINGS.mobileNo, mobileNumberController.text);

        sharedPreferences.commit();

        /*----------loading dismiss----------------*/

      } else {
        if (loginModel.message != null && loginModel.message.isNotEmpty) {
          ShowDialog.dialogs(context, loginModel.message);
        } else {
          ShowDialog.dialogs(context, STRINGS.please_enter_valid_MobileNo);
        }
      }

      /*----------Response Log----------------*/

    } else {
      ShowDialog.dialogs(context, STRINGS.no_internet_connection);
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    final bottom = MediaQuery.of(context).viewInsets.bottom * .60;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: ModalProgressHUD(
            child: _build(height, width, bottom), inAsyncCall: _isLoading),
      ),
    );
  }

  Widget _build(double height, double width, double bottom) {
    return Container(
      height: height,
      width: width,
      color: COLORGLOBLE.ThemeYellow,
      child: ListView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(bottom: bottom),
        shrinkWrap: true,
        //reverse: true,
        //shrinkWrap: true,
        children: <Widget>[
          WidgetSpacer(
            height: height * 0.10,
          ),
          screenLogo(),
          WidgetSpacer(
            height: 50,
          ),
          _isLoginScreen
              ? LoginContainer(height, width)
              : VerifyOtpContainer(height, width),
          //  LoginContainer(height, width)
        ],
      ),
    );
  }

//Login container
  Widget LoginContainer(double height, double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        width: width,
        decoration: boxDecoration(20),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            children: <Widget>[
              Text(
                STRINGS.Login,
                style: FONT.BAUHAUS_BOLD(25, COLORGLOBLE.ThemeBlue),
              ),
              WidgetSpacer(
                height: 20,
              ),
              TextFieldWidget(
                lblText: STRINGS.MobileNumber,
                prefixIconData: Icons.email,
                obscureText: false,
                textEditingController: mobileNumberController,
                keyboardType: TextInputType.number,
              ),
              WidgetSpacer(
                height: 30,
              ),
              new RaisedButton(
                  color: COLORGLOBLE.ThemeBlue,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(50),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 48,
                    child: Text(
                      STRINGS.SendOTP.toUpperCase(),
                      style: FONT.BAUHAUS_BOLD(15, Colors.white),
                    ),
                  ),
                  onPressed: () async {
                    SendOTPValidation();
                  }),
            ],
          ),
        ),
      ),
    );
  }

//varification otp container
  Widget VerifyOtpContainer(double height, double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        width: width,
        decoration: boxDecoration(20),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            children: <Widget>[
              Text(
                STRINGS.VarificationCode,
                style: FONT.BAUHAUS_BOLD(23, COLORGLOBLE.ThemeBlue),
              ),
              WidgetSpacer(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Container(
                  // height: 50,
                  child: PinCodeTextField(
                    length: 4,
                    obsecureText: true,
                    textInputType: TextInputType.number,
                    textStyle: FONT.BAUHAUS_MEDIUM(15, COLORGLOBLE.ThemeBlue),
                    animationType: AnimationType.fade,
                    animationDuration: Duration(milliseconds: 300),

                    //enableActiveFill: true,
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.circle,
                      activeColor: COLORGLOBLE.Gray,
                      borderWidth: 1.5,
                      selectedColor: COLORGLOBLE.ThemeBlue,
                      inactiveColor: COLORGLOBLE.Gray,
                      fieldHeight: 48,
                      fieldWidth: 48,
                    ),
                    controller: otpController,
                  ),
                ),
              ),
              WidgetSpacer(
                height: 25,
              ),
              new RaisedButton(
                  color: COLORGLOBLE.ThemeBlue,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(50),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 48,
                    child: Text(
                      STRINGS.Login.toUpperCase(),
                      style: FONT.BAUHAUS_BOLD(15, Colors.white),
                    ),
                  ),
                  onPressed: () {
                    VarifyOTPValidation();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
