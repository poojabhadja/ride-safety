import 'package:flutter/material.dart';
import 'package:ridesafety/screens/sc_home.dart';
import 'package:ridesafety/screens/sc_login.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/commonFiles/common.dart';
import 'package:ridesafety/utils/string.dart';

import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    checkLogin(context);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        height: height,
        width: width,
        color: COLORGLOBLE.White,
        child: Center(
          child: Container(
            height: height * .50,
            width: width * .70,
            decoration: imageInContainerBoxDecoration("ic_splash.jpeg"),
          ),
        ),
      ),
    );
  }

  void checkLogin(BuildContext context) async {
    SharedPreferences mSharedPreferences;
    String response = "";
    mSharedPreferences = await SharedPreferences.getInstance();
    Future.delayed(Duration(seconds: 3), () {
      response = mSharedPreferences.getString(STRINGS.mobileNo);
      /* UserData streams = UserData.fromJson(json.decode(response));*/
      //print("response--"+response);
      if (response != null && response.isNotEmpty) {
        print("response--" + response);
       Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Home()));
      } else {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false);
      }
    });
  }
}
