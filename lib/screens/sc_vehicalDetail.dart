import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:ridesafety/models/vehicleDetailModel.dart';
import 'package:ridesafety/screens/sc_home.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/commonFiles/RaiseButton.dart';
import 'package:ridesafety/utils/commonFiles/TextField.dart';
import 'package:ridesafety/utils/commonFiles/common.dart';
import 'package:ridesafety/utils/commonFiles/show_dialog.dart';
import 'package:ridesafety/utils/commonFiles/widget_spacer.dart';
import 'package:ridesafety/utils/constants.dart';
import 'package:ridesafety/utils/font.dart';
import 'package:ridesafety/utils/string.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math' as math;

class VehicalDetail extends StatefulWidget {
  @override
  VehicalDetailState createState() {
    return VehicalDetailState();
  }
}

class VehicalDetailState extends State<VehicalDetail>
    with TickerProviderStateMixin {
  VehicleDetailsData _vehicleDetailsData = new VehicleDetailsData();
  bool _isLoading = false;
  AnimationController _controller;
  Animation<double> _animation;
  @override
  Void initState() {
    super.initState();
    _getPreferences();
    _controller = AnimationController(
        duration: const Duration(seconds: 2), vsync: this, value: 0);
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    )..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _controller.forward();
        }
      });

    _controller.forward();
  }

  @override
  Void dispose() {
    super.dispose();
    _controller.dispose();
  }

  Future _getPreferences() async {
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String response = sharedPreferences.getString(STRINGS.vehicleDetailsData);
    if (response != null && response.isNotEmpty) {
      setState(() {
        _vehicleDetailsData =
            VehicleDetailsData.fromJson(json.decode(response));
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    final bottom = MediaQuery.of(context).viewInsets.bottom * .60;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: defaultAppBar(STRINGS.VehicalDetail, true),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: ModalProgressHUD(
            child: _build(height, width, bottom), inAsyncCall: _isLoading),
      ),
    );
  }

  Widget _build(double height, double width, double bottom) {
    return SafeArea(
      bottom: false,
      top: false,
      child: Container(
        height: height,
        width: width,
        color: COLORGLOBLE.ThemeYellow,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Container(
                height: height * .60,
                child: ListView(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    HomeContainer(height, width)
                    // Container(
                    //   //  // height: height * .70,
                    //   //   width: width,
                    //     child: ),
                  ],
                ),
              ),
            ),
            Container(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          STRINGS.ThisRideIsSafe,
                          style: FONT.BAUHAUS_BOLD(25, COLORGLOBLE.Green),
                        ),
                      ),
                      WidgetSpacer(
                        width: 15,
                      ),
                      ScaleTransition(
                        scale: _animation,
                        child: Center(
                          child: Image.asset(
                            "assets/images/ic_safe.png",
                            height: 40,
                            width: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                  WidgetSpacer(
                    height: 20,
                  ),
                  new RaisedButton(
                      color: COLORGLOBLE.ThemeBlue,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        height: 48,
                        child: Text(
                          STRINGS.ScanForPay.toUpperCase(),
                          style: FONT.BAUHAUS_BOLD(15, Colors.white),
                        ),
                      ),
                      onPressed: () {}),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

//Home container
  Widget HomeContainer(double height, double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: COLORGLOBLE.White,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            // physics: BouncingScrollPhysics(),
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: 120,
                width: 120,
                decoration: BoxDecoration(
                    //shape: BoxShape.circle,
                    color: Colors.transparent,
                    image: DecorationImage(
                      image: AssetImage(
                        "assets/images/ic_logo.jpeg",
                      ),
                      //fit: BoxFit.fill
                    )),
              ),
              WidgetSpacer(
                height: 25,
              ),
              _listTileView(
                  STRINGS.VehicalNumber, _vehicleDetailsData.vehicleNo),
              Divider(),
              _listTileView(
                  STRINGS.VehicalType, _vehicleDetailsData.vehicleType),
              Divider(),
              _listTileView(STRINGS.DriverName, _vehicleDetailsData.driverName),
              Divider(),
              _listTileView(
                  STRINGS.DriverContactNo, _vehicleDetailsData.driverMobileNo),
              Divider(),
              _listTileView(STRINGS.PreviousDateOfSanitization,
                  _vehicleDetailsData.senitizeDate),
              Divider(),
              _listTileView(STRINGS.NextDateOfSanitization,
                  _vehicleDetailsData.nextSenitizeDate),
            ],
          ),
        ),
      ),
    );
  }

  Widget _listTileView(String title, String subtitle) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              textAlign: TextAlign.left,
              style: FONT.BAUHAUS_BOLD(15, COLORGLOBLE.DarkGray),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              subtitle == null ? '' : subtitle,
              style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
            ),
          ),
        ],
      ),
    );
  }
}
