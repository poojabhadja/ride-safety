import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ridesafety/models/vehicleDetailModel.dart';
import 'package:ridesafety/screens/sc_login.dart';
import 'package:ridesafety/screens/sc_vehicalDetail.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/commonFiles/RaiseButton.dart';
import 'package:ridesafety/utils/commonFiles/TextField.dart';
import 'package:ridesafety/utils/commonFiles/common.dart';
import 'package:ridesafety/utils/commonFiles/show_dialog.dart';
import 'package:ridesafety/utils/commonFiles/widget_spacer.dart';
import 'package:ridesafety/utils/constants.dart';
import 'package:ridesafety/utils/font.dart';
import 'package:ridesafety/utils/network_repository.dart';
import 'package:ridesafety/utils/string.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  final vehicleNumberController = TextEditingController();
  final vehiacleFocus = FocusNode();
  bool _isLoading = false;
  ScanResult scanResult;
  VehicleDetailsModel vehicleDetailsModel = VehicleDetailsModel();

  void VehicaleValidation() {
    vehiacleFocus.unfocus();
    if (vehicleNumberController.text.isNotEmpty) {
      getVehicaleDetail(context);
    } else {
      ShowDialog.dialogs(context, STRINGS.enter_Your_Vehicle_Number);
    }
  }

/*----------API Calling----------------*/
  void getVehicaleDetail(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        FocusScope.of(context).requestFocus(FocusNode());
        _isLoading = true;
      });

/*----------API url----------------*/
      String url =
          STRINGS.APIGetLastVehicleDetail + "/" + vehicleNumberController.text;

      String response = await callGetMethod(url, null);
/*----------API Response----------------*/
      vehicleDetailsModel = VehicleDetailsModel.fromJson(json.decode(response));
      _isLoading = false;

      if (vehicleDetailsModel.status == STRINGS.STATUS_SUCCESS) {
        setState(() {
          _isLoading = false;
        });
        // /*----------sharedPreferences Data Store----------------*/
        sharedPreferences = await SharedPreferences.getInstance();
        if (vehicleDetailsModel.data == null ||
            vehicleDetailsModel.data.length <= 0) {
          ShowDialog.dialogs(context, STRINGS.no_Data_Found);
        } else {
          sharedPreferences.setString(STRINGS.vehicleDetailsData,
              json.encode(vehicleDetailsModel.data[0]));

          sharedPreferences.commit();

          /*----------loading dismiss----------------*/
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => VehicalDetail()));
        }

        vehicleNumberController.text = "";
      } else {
        if (vehicleDetailsModel.message != null &&
            vehicleDetailsModel.message.isNotEmpty) {
          ShowDialog.dialogs(context, vehicleDetailsModel.message);
        } else {
          ShowDialog.dialogs(context, STRINGS.enter_Valid_Vehicle_Number);
        }
      }
      /*----------Response Log----------------*/
    } else {
      ShowDialog.dialogs(context, STRINGS.no_internet_connection);
    }
  }

  void LogOut() {
    removeAllShareprefrence();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    final bottom = MediaQuery.of(context).viewInsets.bottom * .60;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: ModalProgressHUD(
            child: _build(height, width, bottom), inAsyncCall: _isLoading),
      ),
    );
  }

  Widget _build(double height, double width, double bottom) {
    return Container(
      height: height,
      width: width,
      color: COLORGLOBLE.ThemeYellow,
      child: ListView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(bottom: bottom),
        shrinkWrap: true,
        //reverse: true,
        //shrinkWrap: true,
        children: <Widget>[
          WidgetSpacer(
            height: height * 0.05,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: InkWell(
              onTap: () {
                LogOut();
              },
              child: Container(
                alignment: Alignment.bottomRight,
                child: Icon(
                  Icons.power_settings_new,
                  size: 30,
                  color: COLORGLOBLE.ThemeBlue,
                ),
              ),
            ),
          ),
          WidgetSpacer(
            height: height * 0.05,
          ),
          screenLogo(),
          WidgetSpacer(
            height: 50,
          ),
          HomeContainer(height, width),
          WidgetSpacer(
            height: 50,
          ),

          //  LoginContainer(height, width)
        ],
      ),
    );
  }

//Home container
  Widget HomeContainer(double height, double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        width: width,
        decoration: boxDecoration(20),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            children: <Widget>[
              TextFieldWidget(
                  lblText: STRINGS.VehicalNumber,
                  obscureText: false,
                  textEditingController: vehicleNumberController),
              WidgetSpacer(
                height: 30,
              ),
              new RaisedButton(
                  color: COLORGLOBLE.ThemeBlue,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(50),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 48,
                    child: Text(
                      STRINGS.Next.toUpperCase(),
                      style: FONT.BAUHAUS_BOLD(15, Colors.white),
                    ),
                  ),
                  onPressed: () {
                    VehicaleValidation();
                  }),
              WidgetSpacer(
                height: 30,
              ),
              Center(
                child: Container(
                  child: Text(
                    STRINGS.OR,
                    style: FONT.BAUHAUS_BOLD(20, COLORGLOBLE.ThemeBlue),
                  ),
                ),
              ),
              WidgetSpacer(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0),
                child: new RaisedButton(
                    color: COLORGLOBLE.ThemeBlue,
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(50),
                    ),
                    child: Container(
                      alignment: Alignment.center,
                      height: 48,
                      child: Text(
                        STRINGS.QRCode.toUpperCase(),
                        style: FONT.BAUHAUS_BOLD(15, Colors.white),
                      ),
                    ),
                    onPressed: () async {
                      var result = await BarcodeScanner.scan();
                      vehicleNumberController.text = result.rawContent;
                      print(result.rawContent);
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
