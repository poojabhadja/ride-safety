import 'package:flutter/material.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/font.dart';
import 'package:ridesafety/utils/string.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Design
BoxShadow buttonShadow() {
  return new BoxShadow(
      color: COLORGLOBLE.Black.withOpacity(0.1),
      blurRadius: 6.0,
      spreadRadius: 7.0,
      offset: Offset(0.0, 0.0));
}

BoxShadow boxShadow() {
  return BoxShadow(
    color: COLORGLOBLE.Gray.withOpacity(0.5),
    blurRadius: 10.0,
    spreadRadius: 3.0,
    // offset: Offset(3.0, 3.0)
  );
}

BoxShadow fieldShadow() {
  return BoxShadow(
      color: COLORGLOBLE.Gray.withOpacity(0.5),
      blurRadius: 2.0,
      spreadRadius: .0,
      offset: Offset(3.0, 3.0));
}

BoxDecoration imageInContainerBoxDecoration(String imagenName) {
  return new BoxDecoration(
      image: new DecorationImage(
    image: new AssetImage("assets/images/" + imagenName),
    fit: BoxFit.fill,
  ));
}

BoxDecoration boxDecoration(double radius) {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: COLORGLOBLE.White,
      boxShadow: [buttonShadow()]);
}

Widget fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

AppBar defaultAppBar(String text, bool isCenter) {
  return AppBar(
    title: Text(
      text,
    ),
    centerTitle: isCenter,
   
  );
}

Center screenLogo() {
  return Center(
    child: Container(
      alignment: Alignment.center,
      height: 230,
      width: 230,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            new BoxShadow(
                color: COLORGLOBLE.Black.withOpacity(0.1),
                spreadRadius: 6,
                blurRadius: 5,
                offset: Offset(3, 3))
          ]),
      child: Container(
        height: 170,
        width: 170,
        decoration: BoxDecoration(
            //shape: BoxShape.circle,
            color: Colors.transparent,
            image: DecorationImage(
              image: AssetImage(
                "assets/images/ic_logo.jpeg",
              ),
              //fit: BoxFit.fill
            )),
      ),
    ),
  );
}
//Functionality

void removeAllShareprefrence() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.getKeys();
  for (String key in preferences.getKeys()) {
    preferences.remove(key);
  }
}
