import 'package:flutter/material.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/font.dart';

class TextFieldWidget extends StatelessWidget {
  final String hintText;
  final IconData prefixIconData;
  final String lblText;
//  final IconData suffixIconData;
  final bool obscureText;
  final Function onChanged;
  final FocusNode focusNode;
  final TextEditingController textEditingController;
  final keyboardType ;
  TextFieldWidget(
      {this.hintText,
      this.prefixIconData,
      this.lblText,
//    this.suffixIconData,
      this.obscureText,
      this.onChanged,
      this.textEditingController,
      this.focusNode,
      this.keyboardType});

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      obscureText: obscureText,
      controller: textEditingController,
      cursorColor: COLORGLOBLE.ThemeBlue,
      focusNode: focusNode,
      style: FONT.BAUHAUS_MEDIUM(18, COLORGLOBLE.ThemeBlue),
      keyboardType: keyboardType,
      autocorrect: false,
      
      decoration: InputDecoration(
        labelText: this.lblText,
        
        labelStyle: FONT.BAUHAUS_REGULAR(15, COLORGLOBLE.DarkGray),
        alignLabelWithHint: false,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: COLORGLOBLE.ThemeBlue),
        ),
      ),
    );
  }
}
