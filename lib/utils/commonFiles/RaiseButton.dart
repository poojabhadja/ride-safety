import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/font.dart';
import 'package:ridesafety/utils/string.dart';

class RaisedButtonWidget extends StatelessWidget {
  HexColor color = COLORGLOBLE.ThemeBlue;
  HexColor textColor = COLORGLOBLE.White;
  double borderRadius = 50;
  double height = 48;
  final String text;
  // TextStyle textStyle = FONT.VERDANA_BOLD(15, Colors.white);
  //Function okBtnFunction;
  RaisedButtonWidget({
    this.color,
    this.textColor,
    this.borderRadius,
    this.height,
    this.text,
    // this.textStyle,
    // this.okBtnFunction
  });

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
        color: color,
        textColor: this.textColor,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(this.borderRadius),
        ),
        child: Container(
          alignment: Alignment.center,
          height: this.height,
          child: Text(
            this.text,
            style: FONT.BAUHAUS_BOLD(15, Colors.white),
          ),
        ),
        onPressed: () {
          // okBtnFunction();
        });
  }
}
