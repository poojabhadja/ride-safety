import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/font.dart';

enum DialogAction { yes, abort }

class ShowDialog {
  // Dialog With Okay Button
  static Future<DialogAction> dialogs(BuildContext context, String body) async {
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      return iOSDialogs(context, body);
    } else if (Theme.of(context).platform == TargetPlatform.android) {
      return androidDialogs(context, body);
    }
  }

  // Dialog With Okay (With Action) and Cancel Button
  static Future<DialogAction> dialogWithButtonAction(BuildContext context,
      {@required String title,
      @required String okBtnText,
      @required Function okBtnFunction}) async {
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      return showiOSCustomDialog(context,
          title: title, okBtnText: okBtnText, okBtnFunction: okBtnFunction);
    } else if (Theme.of(context).platform == TargetPlatform.android) {
      return showAndroidCustomDialog(context,
          title: title, okBtnText: okBtnText, okBtnFunction: okBtnFunction);
    }
  }

  static Future<DialogAction> androidDialogs(
      BuildContext context, String body) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            content: Text(
              body,
              style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () =>
                      Navigator.of(context).pop(DialogAction.abort),
                  child: Text(
                    'OK',
                    style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
                  )),
              // RaisedButton(
              //     onPressed: () => Navigator.of(context).pop(DialogAction.yes),
              //     child: const Text('Yes'))
            ],
          );
        });

    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> iOSDialogs(
      BuildContext context, String body) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            // title: new Text(title),
            content: new Text(body),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('OK',
                    style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> showiOSCustomDialog(BuildContext context,
      {@required String title,
      @required String okBtnText,
      @required String cancelBtnText,
      @required Function okBtnFunction}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => new CupertinoAlertDialog(
              content: new Text(
                title,
                style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
              ),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text(
                    "CANCEL",
                    style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                CupertinoDialogAction(
                  child: Text(
                    okBtnText,
                    style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
                  ),
                  onPressed: okBtnFunction,
                ),
              ],
            ));
  }

  static Future<DialogAction> showAndroidCustomDialog(BuildContext context,
      {@required String title,
      @required String okBtnText,
      @required String cancelBtnText,
      @required Function okBtnFunction}) async {
    final action = showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            content: Text(title),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    "CANCEL",
                    style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
                  )),
              FlatButton(
                  onPressed: okBtnFunction,
                  child: Text(
                    okBtnText,
                    style: FONT.BAUHAUS_REGULAR(16, COLORGLOBLE.ThemeBlue),
                  )),
            ],
          );
        });
  }
}
