import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/font.dart';


class NoDataTile extends StatelessWidget {
  String imagePath, title, desc, butText;
  Function okBtnFunction;

  NoDataTile(
      {this.imagePath,
      this.title,
      this.desc,
      this.butText,
      this.okBtnFunction});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          imagePath.isNotEmpty
              ? Image.asset(imagePath,height: 150,width: 150,)
              : SizedBox(
                  height: 0,
                ),
          title.isNotEmpty
              ? SizedBox(
                  height: 40,
                )
              : SizedBox(
                  height: 0,
                ),
          title.isNotEmpty
              ? Text(title,
                  textAlign: TextAlign.center, style: FONT.BAUHAUS_REGULAR(16, Colors.black))
              : SizedBox(
                  height: 0,
                ),
          desc.isNotEmpty
              ? SizedBox(
                  height: 20,
                )
              : SizedBox(
                  height: 0,
                ),
          desc.isNotEmpty
              ? Text(desc,
                  textAlign: TextAlign.center,
                  style: FONT.BAUHAUS_REGULAR(16, Colors.black),)
              : SizedBox(
                  height: 0,
                ),
          butText.isNotEmpty
              ? SizedBox(
                  height: 20,
                )
              : SizedBox(
                  height: 0,
                ),
          butText.isNotEmpty
              ? RaisedButton(
                  color: COLORGLOBLE.Gray,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(7),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 50,
                    child: Text(
                      butText,
                      style: FONT.BAUHAUS_REGULAR(16, Colors.black),
                    ),
                  ),
                  onPressed: () {
                    okBtnFunction();
                  },
                )
              : SizedBox(
                  height: 0,
                ),
        ],
      ),
    );
  }
}
