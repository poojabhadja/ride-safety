import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Constants {
  static const int MobileMax = 10;
  static const int OTPMax = 4;
  static const int PostalCodeMax = 12;
}

SharedPreferences sharedPreferences;

//DateTime stringToDatetime(String dateTime, String dateFormate) {
//  DateFormat format = new DateFormat(dateFormate);
//  return format.parse(dateTime);
//}
