class STRINGS {
  static const String loginData = "loginData";
  static const String app_name = "Ride Safety";
  static const String baseUrl = "http://104.238.93.46:97/";
  static const String internal_server_issue =
      "There might be some internal server issue. Please try after sometime.";
  static const String error_response =
      "{\"status\":\"Error\",\"message\":\"" + internal_server_issue + "\"}";

  static const String STATUS_SUCCESS = '1';
  static const String Error = "Error";

  //Api Error String
  static const String please_enter_your_MobileNo = "Enter Your Mobile Number";
  static const String please_enter_valid_MobileNo = "Enter Valid Mobile Number";
  static const String enter_your_OTP = "Enter Your OTP";
  static const String enter_valid_OTP = "Enter Valid OTP";
  static const String enter_Your_Vehicle_Number = "Enter Your Vehicle Number";
    static const String enter_Valid_Vehicle_Number = "Enter Valid Vehicle Number";
  static const String no_internet_connection =
      "No connectivity to internet. Try again.";
  static const String no_Data_Found =
      "No Data Found";

  // Shared Preferences
  static const String mobileNo = "MobileNo";
   static const String vehicleDetailsData = "vehicaleDetailsData";

//APIs
  static const String APISendOTP = "SendOTP";
  static const String APIGetLastVehicleDetail="GetLastVehicleDetail";

//Basic String
  static const String Login = "Login";
  static const String MobileNumber = "Mobile Number";
  static const String VarificationCode = "Varification Code";
  static const String ResendOTP = "Resend OTP";
  static const String SendOTP = "Send OTP";
  static const String Home = "Home";
  static const String VehicalNumber = "Vehicle Number";
  static const String OR = "OR";
  static const String Next = "Next";
  static const String VehicalDetail = "Vehicle Details";
  static const String VehicalType = "Vehicle Type";
  static const String DriverName = "Driver Name";
  static const String DriverContactNo = "Driver Contact No";
  static const String PreviousDateOfSanitization =
      "Previous Date Of Sanitization";
  static const String NextDateOfSanitization = "Next Date Of Sanitization";
  static const String QRCode="QR Code";
   static const String ScanForPay="Scan For Pay";
   static const String ThisRideIsSafe="This Ride Is Safe";
}
