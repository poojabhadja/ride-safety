import 'package:flutter/material.dart';

class FONT {
   //Verdana
  static BAUHAUS_REGULAR(double fontSize, Color color) {
    return TextStyle(
        fontFamily: 'Bauhaus',
        color: color,
        fontSize: fontSize);
  }

  static BAUHAUS_MEDIUM(double fontSize, Color color) {
    return TextStyle(
        fontFamily: 'Bauhaus',
        fontWeight: FontWeight.w500,
        color: color,
        fontSize: fontSize);
  }

  static BAUHAUS_BOLD(double fontSize, Color color) {
    return TextStyle(
        fontFamily: 'Bauhaus',
        fontWeight: FontWeight.bold,
        color: color,
        fontSize: fontSize);
  }

}
