import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class COLORGLOBLE {
  static final HexColor Black = new HexColor("#000000");
  static final HexColor White = new HexColor("#ffffff");
  static final HexColor Gray = new HexColor("#E7E7E7");
  static final HexColor DarkGray = new HexColor("#969696");
  static final HexColor ThemeBlue = new HexColor("#3C4096");
  static final HexColor ThemeYellow = new HexColor("#FACD33");
  static final HexColor ThemeLightBlue = new HexColor("#b8bbff");
  static final HexColor ThemeLightYellow = new HexColor("#fff0bd");
  static final HexColor Green = new HexColor("#18A560");
}
