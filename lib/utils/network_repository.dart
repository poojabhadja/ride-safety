
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:ridesafety/utils/string.dart';

Future callPostMethod(String url, Map params) async {
  return await http.post(Uri.encodeFull(STRINGS.baseUrl + url),
      body: params,
      headers: {"lancode":"en","platform":"android"}).then((http.Response response) {
      print("URL--" + STRINGS.baseUrl+url + "\nparams--" + params.toString());
    final int statusCode = response.statusCode;
    if (statusCode < 200 || statusCode > 404 || json == null) {
     // throw new Exception("Error while fetching data");
      return STRINGS.error_response;
    }
    print("response--" + response.body);
    return response.body;
  });
}

Future callPostTokenMethod(String url, Map params,String token) async {
  return await http.post(Uri.encodeFull(STRINGS.baseUrl + url),
      body: params,
      headers: {"lancode":"en","platform":"android","token":token}).then((http.Response response) {
      print("URL--" + url + "\nparams--" + params.toString()+"\ntoken--"+token);
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 404 || json == null) {
     // throw new Exception("Error while fetching data");
      return STRINGS.error_response;
    }
      print("--response--" + response.body);
    return response.body;
  });
}
Future callGetMethod(String url, Map params) async {
  return await http.get(Uri.encodeFull(STRINGS.baseUrl + url),
      headers: {"lancode":"en","platform":"android"}).then((http.Response response) {
      print("URL--" + url + "\nparams--" + params.toString());
    final int statusCode = response.statusCode;
    if (statusCode < 200 || statusCode > 404 || json == null) {
     // throw new Exception("Error while fetching data");
      return STRINGS.error_response;
    }
    print("response--" + response.body);
    return response.body;
  });
}
