import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ridesafety/screens/sc_login.dart';
import 'package:ridesafety/screens/sc_splash.dart';
import 'package:ridesafety/utils/colors.dart';
import 'package:ridesafety/utils/font.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor:
            Colors.transparent, // Color for Android // Color for   Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));
    return MaterialApp(
      title: 'Ride Safety',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
        primaryColor: COLORGLOBLE.ThemeBlue, //main theme color
        accentColor: COLORGLOBLE.ThemeBlue, //button colors
        accentColorBrightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: COLORGLOBLE.ThemeBlue,
            textTheme:
                TextTheme(title: FONT.BAUHAUS_BOLD(18, COLORGLOBLE.White))),
      ),
      home: Splash(),
    );
  }
}
