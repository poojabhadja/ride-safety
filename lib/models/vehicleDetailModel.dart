class VehicleDetailsModel {
  String status;
  String message;
  List<VehicleDetailsData> data;

  VehicleDetailsModel({this.status, this.message, this.data});

  VehicleDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    message = json['Message'];
    if (json['Data'] != null) {
      data = new List<VehicleDetailsData>();
      json['Data'].forEach((v) {
        data.add(new VehicleDetailsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['Message'] = this.message;
    if (this.data != null) {
      data['Data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class VehicleDetailsData {
  String stationCode;
  String vehicleNo;
  String senitizeDate;
  double basicAmount;
  double taxAmount;
  double totalAmount;
  String remarks;
  String vehicleStatus;
  String vehicleType;
  String ownerName;
  String ownerMobileNo;
  String driverName;
  String driverMobileNo;
  String ownerAddress;
  String stateCode;
  String nextSenitizeDate;

  VehicleDetailsData(
      {this.stationCode,
      this.vehicleNo,
      this.senitizeDate,
      this.basicAmount,
      this.taxAmount,
      this.totalAmount,
      this.remarks,
      this.vehicleStatus,
      this.vehicleType,
      this.ownerName,
      this.ownerMobileNo,
      this.driverName,
      this.driverMobileNo,
      this.ownerAddress,
      this.stateCode,
      this.nextSenitizeDate});

  VehicleDetailsData.fromJson(Map<String, dynamic> json) {
    stationCode = json['StationCode'];
    vehicleNo = json['VehicleNo'];
    senitizeDate = json['SenitizeDate'];
    basicAmount = json['BasicAmount'];
    taxAmount = json['TaxAmount'];
    totalAmount = json['TotalAmount'];
    remarks = json['Remarks'];
    vehicleStatus = json['VehicleStatus'];
    vehicleType = json['VehicleType'];
    ownerName = json['OwnerName'];
    ownerMobileNo = json['OwnerMobileNo'];
    driverName = json['DriverName'];
    driverMobileNo = json['DriverMobileNo'];
    ownerAddress = json['OwnerAddress'];
    stateCode = json['StateCode'];
    nextSenitizeDate = json['NextSenitizeDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['StationCode'] = this.stationCode;
    data['VehicleNo'] = this.vehicleNo;
    data['SenitizeDate'] = this.senitizeDate;
    data['BasicAmount'] = this.basicAmount;
    data['TaxAmount'] = this.taxAmount;
    data['TotalAmount'] = this.totalAmount;
    data['Remarks'] = this.remarks;
    data['VehicleStatus'] = this.vehicleStatus;
    data['VehicleType'] = this.vehicleType;
    data['OwnerName'] = this.ownerName;
    data['OwnerMobileNo'] = this.ownerMobileNo;
    data['DriverName'] = this.driverName;
    data['DriverMobileNo'] = this.driverMobileNo;
    data['OwnerAddress'] = this.ownerAddress;
    data['StateCode'] = this.stateCode;
    data['NextSenitizeDate'] = this.nextSenitizeDate;
    return data;
  }
}