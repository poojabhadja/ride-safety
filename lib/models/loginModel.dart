class LoginModel {
  String status;
  String message;
  LoginData data;

  LoginModel({this.status, this.message, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    message = json['Message'];
    data = json['Data'] != null ? new LoginData.fromJson(json['Data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['Message'] = this.message;
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    return data;
  }
}

class LoginData {
  String oTP;
  String mobileNo;
  String defOTP;

  LoginData({this.oTP, this.mobileNo, this.defOTP});

  LoginData.fromJson(Map<String, dynamic> json) {
    oTP = json['OTP'];
    mobileNo = json['MobileNo'];
    defOTP = json['DefOTP'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OTP'] = this.oTP;
    data['MobileNo'] = this.mobileNo;
    data['DefOTP'] = this.defOTP;
    return data;
  }
}
